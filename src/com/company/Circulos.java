package com.company;

public class Circulos implements CalcularArea {
    private int raio;

    @Override
    public double area() {
        return 3.14*raio*raio;
    }

    public int getRaio() {
        return raio;
    }

    public void setRaio(int raio) {
        this.raio = raio;
    }
}

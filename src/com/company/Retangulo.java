package com.company;

public class Retangulo implements CalcularArea{
    private double lagura,altura;
    @Override
    public double area() {
        return this.lagura*this.altura;
    }



    public void setLagura(double lagura) {
        this.lagura = lagura;
    }



    public void setAltura(double altura) {
        this.altura = altura;
    }
}

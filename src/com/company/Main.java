//1- Crie um sistema que permite a criação de formas geométricas e o cálculo de suas áreas. O sistema deve atender os seguintes requisitos:
//Deve se ter como formas obrigatórias o círculo, o triângulo e o retângulo.
//Para criar uma forma, basta que seja informado o tamanho de seus lados.
//Após criada uma forma, não é possível alterar o tamanho de seus lados.
//No caso do triângulo, deve-se verificar se os lados informados geram um triângulo válido. Caso contrário, responder um erro no sout
//Utilizar o próprio código fonte para instanciar 3 formas e exibir suas áreas no console.
//2- Aprimorar o sistema para que o usuário possa informar, através do terminal, qual forma deseja criar e o tamanho de seus lados.
//Caso o usuário informe mais de 4 lados, responder um erro no terminal.

package com.company;

public class Main {

    public static void main(String[] args) {

        //obtem forma
        Forma forma = new Forma();
        forma.setQtelados(1);
        System.out.println(forma.retornaForma());
        //------------------------
        //Area Circulo
        Circulos area = new Circulos();
        area.setRaio(2);
        double _area = area.area();
        System.out.println("area circulo:" + _area);

        //Area retangulo
        Retangulo ret = new Retangulo();
        ret.setAltura(2);
        ret.setLagura(3);
        _area = ret.area();
        System.out.println("area retangulo:" + _area);

        //Area triangulo
        Triangulo triangulo = new Triangulo();
        triangulo.setLadoA(3);
        triangulo.setLadoB(7);
        triangulo.setLadoC(6);
        boolean btriangulo = triangulo.isTrianguloValido();
        System.out.println("trinagulo valido:" + btriangulo);
        _area = triangulo.area();
        System.out.println("area triangulo:" + _area);
    }
}

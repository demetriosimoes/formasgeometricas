package com.company;

public class Triangulo implements CalcularArea{
    private double ladoA,ladoB,ladoC;
    @Override
    public double area() {
        double s = (this.ladoA+this.ladoB+this.ladoC)/2;
        return Math.sqrt(s*(s-this.ladoA)*(s-this.ladoB)*(s-this.ladoC));
    }

    public void setLadoA(int ladoA) {
        this.ladoA = ladoA;
    }

    public void setLadoB(int ladoB) {
        this.ladoB = ladoB;
    }

    public void setLadoC(int ladoC) {
        this.ladoC = ladoC;
    }
    public boolean isTrianguloValido(){
        if( ((this.ladoA+this.ladoB)> this.ladoC &&
                (this.ladoA+this.ladoC)> this.ladoB &&
                (this.ladoB+this.ladoC)> this.ladoA) ==true
        ){
            return true;
        }else {
            return false;
        }
    }

}

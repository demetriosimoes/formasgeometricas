package com.company;

import java.util.ArrayList;

public class Forma {
    private int qtelados;
    private String forma;
    private ArrayList<String> dadoArray = new ArrayList<String>();

    public Forma() {
        this.dadoArray.add("Circulo");
        this.dadoArray.add("Retangulo");
        this.dadoArray.add("Triangulo");
    }

    public int getQtelados() {
        return qtelados;
    }

    public void setQtelados(int qtelados) {
        this.qtelados = qtelados;
    }

    public String getForma() {
        return forma;
    }

    public void setForma(String forma) {
        this.forma = forma;
    }

    public String retornaForma(){
        if((this.qtelados>3) || (this.qtelados==0)){
            return "nao definido";
        }else {
            return dadoArray.get(this.qtelados - 1);
        }
    }
}
